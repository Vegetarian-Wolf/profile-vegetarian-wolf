import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <div className="App d-flex justify-content-center align-items-center text-center bg-dark text-danger">
            My name is Vegetarian Wolf
        </div>
    );
}

export default App;
