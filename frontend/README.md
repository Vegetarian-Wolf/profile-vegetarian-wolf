I. Setup project reactjs
1) npx create-react-app frontend // name-project
2) I think you should choose the framework css : Bootstrap 5 or Ant Design 
- cd frontend -> npm install react-bootstrap bootstrap or npm install antd
3) react-router-dom@6(router)
- cd frontend -> npm install react-router-dom@6
- the different between Element and Component:
*** Element || Component ***
- An element is always gets returned by a component. || A component can be functional or a class that optionally takes input and returns an element.
- The element does not have any methods. || Each component has its life cycle methods.
- A React element is an object representation of a DOM node. || A component encapsulates a DOM tree.
- Elements are immutable i,e once created cannot be changed. || The state in a component is mutable.
- An element can be created using React.createElement( ) with type property. || A component can be declared in different ways like it can be an element class with render() method or can be defined as a function.
- We cannot use React Hooks with elements as elements are immutable. || React hooks can be used with only functional components.
- Elements are light, stateless and hence it is faster. || It is comparatively slower than elements.
4) cd frontend -> npm run start -> http://localhost:3000
5) delete unnecessary junk files
6) format document = alt + shift + f( I like spaces 4, very easy to watch)
7) gitlab -> 